create or replace function get_cust_birth (v_social_type varchar,
                                           v_social_id varchar,
                                           out birth date)
returns date as
$body$
declare
  v_date_string varchar;
begin

    if v_social_id is null or v_social_type is null or v_social_type != '01' then
      birth := NULL;
      return;
    else
      if char_length(v_social_id) not in (15, 18) then
        birth := NULL;
        return;
      elsif char_length(v_social_id) = 18 then
        v_date_string := substring(v_social_id, 7, 8);
      elsif char_length(v_social_id) = 15 then
        v_date_string := '19' || substring(v_social_id, 7, 6);
      end if;
    end if;

    if v_date_string !~ '\d{8}' then
      birth := NULL;
      return;
    else
      birth := to_date(v_date_string, 'yyyymmdd');
    end if;

    if v_date_string != to_char(birth, 'yyyymmdd') then
      birth := NULL;
      return;
    end if;

    if (birth < '19000101'::date) or (birth > current_date) then
      birth := NULL;
      return;
    end if;

    return;
end;
$body$
language plpgsql;
