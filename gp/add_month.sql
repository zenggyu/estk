create or replace function add_month(v_yyyymm int, v_month int, out yyyymm int)
returns int as
$body$
begin
  yyyymm := to_char(to_date(v_yyyymm::varchar, 'yyyymm') + (v_month::varchar || ' month')::interval, 'yyyymm')::int;
  return;
end;
$body$
language plpgsql;
