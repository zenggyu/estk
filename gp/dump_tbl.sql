CREATE OR REPLACE FUNCTION dump_tbl(v_tbl VARCHAR, v_path VARCHAR)
  RETURNS VOID AS $body$
DECLARE
  v_sql  TEXT;
  v_file TEXT;
BEGIN
  IF (substring(v_path, '(.)$') != '/')
  THEN
    RAISE EXCEPTION 'Path must end with a slash (/).';
  ELSE
    v_file = v_path || v_tbl || '.' || current_user || '.csv';
    v_sql = 'COPY ' || v_tbl || E' TO ''' || v_file || ''' (FORMAT CSV, HEADER TRUE);';
    EXECUTE v_sql;
  END IF;
END;
$body$
LANGUAGE plpgsql;
