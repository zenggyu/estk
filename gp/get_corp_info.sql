create or replace function get_corp_info (out head varchar, out name varchar, out id varchar) returns record as $body$
  select substring(current_user, 'summary_(.*)') as head, corp_name as name, corp_id as id
  from metadata.edp_dim_corp
  where corp_head = substring(current_user, 'summary_(.*)');
$body$ language sql;
