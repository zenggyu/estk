#!/bin/bash

# This script synchronizes a file among various ELT servers.

# Options:
# -f: the file to be synchronized

while getopts 'f:' opt; do
  if [[ $opt == 'f' ]]; then
    file=$OPTARG
    if [[ -z $file ]]; then
      echo 'Please specify option -f.'
    elif ! [[ -f $file ]]; then
      echo "File $file does not exist."
      exit 1
    fi
  fi
done

server_list='132.121.126.130 132.121.126.131 132.121.126.132'

if ! [[ $file =~ ^/ ]]; then
  file=$PWD/$file
fi

for x in $server_list; do
  echo "Moving file $file to $x:$file at $(date)."
  scp "$file" "$x":"$file"
done
