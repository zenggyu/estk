#!/bin/bash

# This script executes an SQL command (either through the -f or the -c option) in multiple databases.

# Options:
# -d: database, can be one of the following forms (xx is the abbreviation of a database, such as gz, fs, etc.):
#     - all: run the command in all databases
#     - xx,xx,xx[,...]: run the command in xx,xx,xx,...
#     - -xx,xx,xx[,...]: run the command in all databases except xx,xx,xx,...
# -f: SQL script file
# -c: SQL command

while getopts 'd:f:c:' opt; do
  if [[ $opt == 'd' ]]; then
    database=$OPTARG
    if [[ -z $database ]]; then
      echo 'Please specify option -d.'
      exit 1
    fi
  elif [[ $opt == 'f' ]]; then
    file=$OPTARG
    if [[ -z $file ]]; then
      echo 'Please specify option -f.'
      exit 1
    fi
    if ! [[ $file =~ ^/ ]]; then
      file=$PWD/$file
    fi
    if ! [[ -f $file ]]; then
      echo "File $file does not exist."
      exit 1
    fi
  elif [[ $opt == 'c' ]]; then
    command=$OPTARG
    if [[ -z $command ]]; then
      echo "Please specify option -c"
      exit 1
    fi
  fi
done

if [[ (-z $file && -z $command) || (-n $file && -n $command) ]]; then
  echo "One and only one of -f and -c option should be specified."
  exit 1
fi

database_list='jm yf yj gz zj mm cz hz st sw fs mz zq sg jy hy sz qy zh zs dg'

if [[ $database == 'all' ]]; then
  database=$database_list
elif [[ $database =~ ^.{2}(,.{2})*$ ]]; then
  database=$(echo $database | sed 's/,/ /g')
elif [[ $database =~ ^-.{2}(,.{2})*$ ]]; then
  exclude=$(echo $database | sed 's/-//g' | sed 's/,/ /g')
  database=$database_list
  for x in $exclude; do
    database=$(echo $database | sed "s/$x//g")
  done
fi

for x in $database; do
  if ! [[ $database_list =~ $x ]]; then
    echo "Invalid input: $x."
    exit 1
  fi
done

for x in $database; do
  cluster1='jm yf yj gz'
  cluster2='zj mm cz hz st sw fs'
  cluster3='mz zq sg jy hy sz'
  cluster4='qy zh zs dg'
  echo "----------Executing command $file in $x at $(date)...----------"
  if [[ $cluster1 =~ $x ]]; then
    if [[ -n $file ]]; then
      psql -d eda_gp -U summary_$x -h 132.121.126.171 -f "$file"
    else
      psql -d eda_gp -U summary_$x -h 132.121.126.171 -c "$command"
    fi
  elif [[ $cluster2 =~ $x ]]; then
    if [[ -n $file ]]; then
      ssh 132.121.126.130 "psql -d eda_gp -U summary_$x -h 132.121.126.171 -f "\'$file\'
    else
      ssh 132.121.126.130 "psql -d eda_gp -U summary_$x -h 132.121.126.171 -c "\'$command\'
    fi
  elif [[ $cluster3 =~ $x ]]; then
    if [[ -n $file ]]; then
      ssh 132.121.126.131 "psql -d eda_gp -U summary_$x -h 132.121.126.172 -f "\'$file\'
    else
      ssh 132.121.126.131 "psql -d eda_gp -U summary_$x -h 132.121.126.172 -c "\'$command\'
    fi
  elif [[ $cluster4 =~ $x ]]; then
    if [[ -n $file ]]; then
      ssh 132.121.126.132 "psql -d eda_gp -U summary_$x -h 132.121.126.172 -f "\'$file\'
    else
      ssh 132.121.126.132 "psql -d eda_gp -U summary_$x -h 132.121.126.172  -c "\'$command\'
    fi
  fi
done
